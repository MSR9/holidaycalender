//
//  Holiday.swift
//  HolidayCalender
//
//  Created by EPITADMBP04 on 3/24/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation

struct HolidayResponse: Decodable {
    var response: Holidays
}

struct Holidays: Decodable {
    var holidays:[HolidayDetail]
}

struct HolidayDetail: Decodable {
    var name : String
    var date : DateInfo
}

struct DateInfo: Decodable{
    var iso : String
}
